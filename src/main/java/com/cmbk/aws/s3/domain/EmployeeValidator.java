package com.cmbk.aws.s3.domain;

import org.springframework.util.Assert;

/**
 * @author chanaka.k
 *
 */
public class EmployeeValidator {
	
	private EmployeeValidator() {
	}

	public static void validateCreateEmployeeRequest(CreateEmployeeRequest request) {
		Assert.notNull(request, "Create Employee Request cannot be null");
		Assert.hasText(request.getFirstName(), "First name is required");
		Assert.notNull(request.getSallary(), "Sallary is required");
	}

}
