package com.cmbk.aws.s3.domain.transform;

import java.util.UUID;

import com.cmbk.aws.s3.domain.CreateEmployeeRequest;
import com.cmbk.aws.s3.domain.Employee;
import com.cmbk.aws.s3.domain.EmployeeResponse;

/**
 * @author chanaka.k
 *
 */
public class EmployeeTransformer {

	public static Employee transformEmployeeRequestToDto(CreateEmployeeRequest createEmployeeRequest) {

		Employee employee = new Employee();
		employee.setFirstName(createEmployeeRequest.getFirstName());
		employee.setJoinedDate(createEmployeeRequest.getJoinedDate());
		employee.setLastName(createEmployeeRequest.getLastName());
		employee.setSallary(createEmployeeRequest.getSallary());
		employee.setId(UUID.randomUUID().toString());

		return employee;
	}


	public static EmployeeResponse employeeToEmployeeResponse(Employee employee) {

		EmployeeResponse employeeResponse = new EmployeeResponse();
		employeeResponse.setFirstName(employee.getFirstName());
		employeeResponse.setJoinedDate(employee.getJoinedDate());
		employeeResponse.setLastName(employee.getLastName());
		employeeResponse.setSallary(employee.getSallary());

		return employeeResponse;
	}

}
