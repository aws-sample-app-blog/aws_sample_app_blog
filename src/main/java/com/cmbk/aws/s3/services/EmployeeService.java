package com.cmbk.aws.s3.services;

import com.cmbk.aws.s3.domain.CreateEmployeeRequest;
import com.cmbk.aws.s3.domain.CreateEmployeeResponse;
import com.cmbk.aws.s3.domain.EmployeeResponse;

/**
 * @author chanaka.k
 *
 */
public interface EmployeeService {

	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest);

	public EmployeeResponse retrieveEmployeeById(String employeeID);

	public void removeEmployeeById(String employeeId);

}
