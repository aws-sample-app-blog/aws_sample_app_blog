package com.cmbk.aws.s3.infrastructure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

/**
 * @author chanaka.k
 *
 */
@Component
public class AWSConfig {

	/** The log. */
	private final Logger log = LogManager.getLogger(getClass());

	/** The aws region. */
	@Value("${aws.region}")
	private String awsRegion;

	/** The auth key root. */
	@Value("${file.location.s3}")
	private String bucketName;

	/** The s 3 client. */
	private AmazonS3 s3Client;

	/**
	 * Initialization.
	 */
	@PostConstruct
	public void init() {
		s3Client = this.getAmazonS3Client();
	}

	/**
	 * Gets the amazon S 3 client.
	 *
	 * @return the amazon S 3 client
	 */
	private AmazonS3 getAmazonS3Client() {

		Regions configRegion = Regions.valueOf(awsRegion);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(configRegion).build();

		return s3Client;
	}

	/**
	 * Retrieve object content as string.
	 *
	 * @param objectKey the object key
	 * @return the string
	 */
	public String retrieveFileContentAsString(String objectKey) {

		try {

			Assert.hasText(objectKey, "Object key cannot be empty.");

			S3Object s3Object = s3Client.getObject(new GetObjectRequest(bucketName, objectKey));

			return getAsString(s3Object);

		} catch (IllegalArgumentException e) {
			log.error("Error : " + e.getMessage());
			throw e;
		} catch (AmazonS3Exception e) {
			log.error("Error : " + e.getMessage());
			// check for keys not found
			if (e.getStatusCode() == 404) {
				throw new RuntimeException("S3 object not found for: " + objectKey, e);
			} else {
				throw new RuntimeException(e.getErrorMessage(), e);
			}
		} catch (Exception e) {
			log.error("Error : " + e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * Gets the as string.
	 *
	 * @param s3Object the s 3 object
	 * @return the as string
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private static String getAsString(S3Object s3Object) throws IOException {

		if (s3Object == null || s3Object.getObjectContent() == null) {
			return null;
		}
		S3ObjectInputStream is = s3Object.getObjectContent();
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, com.amazonaws.util.StringUtils.UTF8));
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} finally {
			is.close();
		}
		return sb.toString();
	}

	/**
	 * Retrieve object content as string.
	 *
	 * @param objectKey the object key
	 * @return the string
	 */
	public void uploadFile(String objectKey, String content) {

		try {

			Assert.hasText(objectKey, "Object key cannot be empty.");
			Assert.notNull(content, "Content cannot be NULL.");

			s3Client.putObject(bucketName, objectKey, content);

		} catch (IllegalArgumentException e) {
			log.error("Error : " + e.getMessage());
			throw e;
		} catch (AmazonS3Exception e) {
			log.error("Error : " + e.getMessage());
			// check for keys not found
			if (e.getStatusCode() == 404) {
				throw new RuntimeException("S3 object not found for: " + objectKey, e);
			} else {
				throw new RuntimeException(e.getErrorMessage(), e);
			}
		} catch (Exception e) {
			log.error("Error : " + e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * Remove object content.
	 *
	 * @param objectKey  the object key
	 * @param bucketName the bucket name
	 * @return void
	 */
	public void removeFileById(String objectKey) {

		try {

			Assert.notNull(objectKey, "Object key cannot be NULL/empty.");

			s3Client.deleteObject(bucketName, objectKey);

		} catch (Exception e) {
			log.error("Error occured while deleting the file : " + e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}

	}

}
