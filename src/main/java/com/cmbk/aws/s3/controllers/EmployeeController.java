package com.cmbk.aws.s3.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmbk.aws.s3.controllers.common.HTTPResponseHandler;
import com.cmbk.aws.s3.controllers.common.RequestMappings;
import com.cmbk.aws.s3.domain.CreateEmployeeRequest;
import com.cmbk.aws.s3.domain.CreateEmployeeResponse;
import com.cmbk.aws.s3.domain.EmployeeResponse;
import com.cmbk.aws.s3.domain.EmployeeValidator;
import com.cmbk.aws.s3.services.EmployeeService;

/**
 * @author chanaka.k
 *
 */
@RestController
@RequestMapping(RequestMappings.EMPLOYEES)
public class EmployeeController extends HTTPResponseHandler {

	final static Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	/**
	 * Create new employee object file and push into the S3 bucket
	 *
	 */
	@RequestMapping(value = RequestMappings.CREATE_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody CreateEmployeeResponse createEmployee(@RequestBody CreateEmployeeRequest request) {

		EmployeeValidator.validateCreateEmployeeRequest(request);

		logger.info("Inside > create new employee endpoint ");

		return employeeService.createEmployee(request);

	}

	/**
	 * Retrieve employee by employee id from S3 bucket
	 *
	 */
	@RequestMapping(value = RequestMappings.RETRIVE_EMPLOYEE_BY_ID, method = RequestMethod.GET)
	public @ResponseBody EmployeeResponse retriveEmployeeById(
			@PathVariable(value = "employee-id", required = true) String employeeId) {

		logger.info("Inside > retrieve employee by id endpoint ");

		return employeeService.retrieveEmployeeById(employeeId);

	}

	/**
	 * Remove employee by employee id from S3 bucket
	 *
	 */
	@RequestMapping(value = RequestMappings.REMOVE_EMPLOYEE, method = RequestMethod.DELETE)
	public void removeEmployeeById(@PathVariable(value = "employee-id", required = true) String employeeId) {

		logger.info("Inside > remove employee by id endpoint ");

		employeeService.removeEmployeeById(employeeId);

	}
}
